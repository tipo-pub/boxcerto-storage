<section class="sidebar-right background-grey">
    <div class="container">

        <div class="text-center m-b-60">
            <h3 class="text-medidas">Escolha o melhor box para você</h3>
        </div>

        <div class="row">
            <div class="content col-lg-12">
                <div class="tabs">
                    <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                        <?php /*
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#1m" role="tab" aria-controls="1m" aria-selected="true">1m²</a>
                        </li>
                        */?>
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#2m" role="tab" aria-controls="2m" aria-selected="true">2m²</a>
                        </li>
                        <li class="nav-item icon-box text-center effect large light">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#3m" role="tab" aria-controls="3m" aria-selected="false">3m²</a>
                        </li>
                        <li class="nav-item icon-box text-center effect large light">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#4m" role="tab" aria-controls="4m" aria-selected="false">4m²</a>
                        </li>
                        <li class="nav-item icon-box text-center effect large light">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#5m" role="tab" aria-controls="5m" aria-selected="false">5m²</a>
                        </li>
                        <li class="nav-item icon-box text-center effect large light">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#6m" role="tab" aria-controls="6m" aria-selected="false">6m²</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <?php /*
                        <!-- MEDIDA - 1M²-->
                        <div class="tab-pane fade show active" id="1m" role="tabpanel" aria-labelledby="home-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/1m.png" alt="Medida 1m²">
                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
<!--
                                        <div class="m-b-40">
                                            <h3>2m² - 1 x 2 x 2,70</h3>

                                            <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                <li>1 geladeira</li>
                                                <li>1 fogão</li>
                                                <li>1 microondas</li>
                                                <li>1 mesa pequena</li>
                                                <li>4 cadeiras</li>
                                            </ul>

                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>
-->

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 1M²-->
                        */?>

                        <!-- MEDIDA - 2M²-->
                        <div class="tab-pane fade show active" id="2m" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/2m.png" alt="Medida 2m²">
                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
                                        <div class="m-b-40">
                                            <h3>2m² - 1 x 2 x 2,70</h3>

                                            <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                <li>1 geladeira</li>
                                                <li>1 fogão</li>
                                                <li>1 microondas</li>
                                                <li>1 mesa pequena</li>
                                                <li>4 cadeiras</li>
                                            </ul>

                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 2M²-->

                        <!-- MEDIDA - 3M²-->
                        <div class="tab-pane fade" id="3m" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/3m.png" alt="medida 3m²">

                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
                                        <div class="m-b-40">
                                            <h3>2 (larg) x 1,5 (profund) x 2,6 (alt)</h3>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 Geladeira</li>
                                                        <li>1 Fogão</li>
                                                        <li>1 Micro-ondas</li>
                                                    </ul>
                                                </div>


                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 Mesa pequena</li>
                                                        <li>4 Cadeiras</li>
                                                        <li>1 Sofá de 3 lugares</li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 3M²-->

                        <!-- MEDIDA - 4M²-->
                        <div class="tab-pane fade" id="4m" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/4m.png" alt="Medida 4m²">

                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
                                        <div class="m-b-40">
                                            <h3>2 (larg) x 2 (profund) x 2,6 (alt)</h3>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 geladeira</li>
                                                        <li>1 fogão</li>
                                                        <li>1 máquina de lavar</li>
                                                        <li>1 micro-ondas</li>
                                                    </ul>
                                                </div>

                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 mesa média</li>
                                                        <li>6 cadeiras</li>
                                                        <li>1 sofá de 3 lugares</li>
                                                        <li>1 cama box solteiro</li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 4M²-->

                        <!-- MEDIDA - 5M²-->
                        <div class="tab-pane fade" id="5m" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/5m.png" alt="Medida 5m²">

                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
                                        <div class="m-b-40">
                                            <h3>2,5 (larg) x 2 (profund) x 2,65 (alt)</h3>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 geladeira</li>
                                                        <li>1 fogão</li>
                                                        <li>1 máquina de lavar</li>
                                                        <li>1 microondas</li>
                                                    </ul>
                                                </div>

                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 mesa média</li>
                                                        <li>6 cadeiras</li>
                                                        <li>1 sofá de 2 lugares</li>
                                                        <li>1 cama box casal</li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 5M²-->

                        <!-- MEDIDA - 6M²-->
                        <div class="tab-pane fade" id="6m" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="item-box">

                                <div class="row">
                                    <div class="col-lg-7 m-b-30" data-animation="fadeInLeft">

                                        <div class="img-box text-center">
                                            <img src="images/6m.png" alt="Medida 6m²">

                                        </div>

                                    </div>
                                    <div class="col-lg-5 p-l-40 p-r-40 mt-4" data-animation="fadeInRight">
                                        <div class="m-b-40">
                                            <h3>3 (larg) x 2 (profund) x 2,6 (alt)</h3>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 Geladeira</li>
                                                        <li>1 Fogão</li>
                                                        <li>1 Máquina de lavar</li>
                                                        <li>1 Micro-ondas</li>
                                                        <li>1 Mesa média</li>
                                                        <li>6 Cadeiras</li>
                                                    </ul>
                                                </div>

                                                <div class="col-md-6">
                                                    <ul class="list-icon list-icon-check text-dark list-icon-colored">
                                                        <li>1 Sofá de 3 lugares</li>
                                                        <li>1 Sofá de 2 lugares</li>
                                                        <li>1 Cama box casal</li>
                                                        <li>1 Cama box solteiro</li>
                                                        <li>1 Cômoda</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                            <a href="<?=$url;?>#"><button type="button" class="btn btn-shadow">Solicitar orçamento</button></a>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM / MEDIDA - 6M²-->

                    </div>
                </div>



            </div>

        </div>
    </div>
</section>
