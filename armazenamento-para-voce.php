<?php
include "includes/geral.php";
$title = 'Para você';
$description = '';
$keywords = '';
include "includes/head.php";
include "includes/header.php";   
    // include "includes/slider.php";   
?>
<section id="solutions">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 pb-5">
        <div class="slider-solutions">
          <img class="" src="images/para-voce/docas.jpg" alt="">
          <img class="" src="images/para-voce/recepcao-frente.jpg" alt="">
          <img class="" src="images/para-voce/recepcao-tras.jpg" alt="">
          <img class="" src="images/para-voce/frente-da-empresa-parte-interna.jpg" alt="">
          <img class="" src="images/para-voce/biometria-exterior.jpg" alt="">
      </div>
    </div>
      <div class="col-12 col-lg-6 pl-5">
        <h3>Soluções em Self Storage / Guarda Móveis</h3>
        <p>O Self Storage / Guarda Móveis é utilizado para armazenamento de móveis e volumes em caso de mudanças, reformas, viagens longas ou simplesmente para ter uma extensão de sua residência.</p>

        <h4>Vantagens</h4>
        <ul>
          <li>Móveis acondicionados em local adequado para armazenagem;</li>
          <li>Privacidade: somente você ou pessoas autorizadas por você terão acesso ao box;</li>
          <li>Dedetização periódica;</li>
          <li>Monitoramento por câmeras, 24 horas por dia e sistema de alarme;</li>
          <li>Contrato sem burocracia e sem fiador;</li>
          <li>Controle de acesso por biométrico e cartão RFID.</li>
        </ul>
      </div>
    </div>
  </div>
</section>



<?php include 'includes/footer.php' ;?>
