<?php if(isset($linkPagina) && ($linkPagina == 'index') || ($linkPagina == '')): ?>

<?php	else: ?>

	<section id="breadcrumb">
		<div class="container">
			<div class="row">
					<div class="col-12 col-lg-6 pt-4">
						<p class="bread-title"><?=$title;?></p>
					</div>
					<div class="col-12 col-lg-6 pb-4 pt-lg-4 pb-lg-0">
						<ul class="breadcrumb breadcrumb-light d-flex bread-title items-breadcrumb">
							<?=breadcrumb()?> 
						</ul>
					</div>
				</div>
			</div>
		
	</section>
	<?php endif; ?>