

<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider slider-fullscreen  arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide background-image" style="background-image:url('images/img-banner-home-1.jpg');">
        <div class="container">
            <div class="slide-captions text-left">

                <span class="strong fadeInUp" style="animation-duration: 600ms;">PRECISANDO DE ESPAÇO?</span>
                <h1 class="fadeInUp" style="animation-duration: 600ms;">Aluguel de box<br> de 2m² a 6m²</h1>
                <a class="btn fadeInUp" href="<?=$url;?>#" title="Orçamento" style="animation-duration: 600ms;">Orçamento</a>

            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide background-image" style="background-image:url('images/img-banner-home-2.jpg');">
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h1 class="t-m">Monitoramento por câmeras 24h/dia</h1>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
    <!-- Slide 3 -->
    <div class="slide background-image" style="background-image:url('images/img-banner-home-1.jpg');">
        <div class="container">
            <div class="slide-captions text-left">
                <!-- Captions -->
                <h1 class="text-medium m-b-3">RESERVE JÁ O SEU</h1>
                <a class="btn fadeInUp" href="<?=$url;?>#" title="Contato" style="animation-duration: 600ms;">Contato</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 3 -->

</div>
<!--end: Inspiro Slider -->
