<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!-- Document title -->
    <title><?=$title;?> | <?=$NomeEmpresa;?></title>
    
    <!-- SEO -->
    <meta name="title" content="<?=$title?>" />
    <meta name="description" content="<?=$description;?>">
    <meta name="keywords" content="<?=$keywords;?>" />
    <meta name="language" content="PT-BR" />

    <meta name="author" content="<?=$author;?>">
    <meta name="copyright" content="<?=$NomeEmpresa;?>" />
    <meta name="distribution" content="global" />
    <meta name="audience" content="all" />
    <meta name="url" content="<?=$url;?>" />
    <meta name="classification" content="<?=$ramo;?>" />
    <meta name="category" content="<?=$ramo;?>" />
    <meta name="Page-Topic" content="<?=$title?>" />
    <meta name="rating" content="general" />
    <meta name="fone" content="<?='' . $ddd . ' ' . $tel;?>" />
    <meta name="city" content="<?=$uf;?>" />
    <meta name="country" content="Brasil" />
    <meta property="publisher" content="<?=$creditos;?>" />

    <!-- Google -->
    <link rel="canonical" href="<?=$canonical;?>" />
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="index,follow" />
    <meta name="geo.placename" content="Brasil" />
    <meta name="geo.region" content="<?=$uf;?>" />
    <meta itemprop="name" content="<?=$NomeEmpresa;?>" />
    <meta itemprop="description" content="<?=$description;?>" />
    <meta itemprop="image" content="<?=$card;?>" />

    <!-- Facebook -->
    <meta property="og:title" content="<?=$title?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=$canonical;?>" />
    <meta property="og:site_name" content="<?=$NomeEmpresa?>">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:image" content="<?=$card;?>">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:image:width" content="470">
    <meta property="og:image:height" content="256">
    <meta property="og:description" content="<?=$description;?>">
    <meta property="fb:admins" content="<?=$CodFanpage;?>" />
    <link href="<?=$card;?>" rel="image_src" />
    
    <!-- Stylesheets & Fonts -->
    <link rel="preload" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

    <link href="dist/style.css" rel="stylesheet">

     
     <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff2" href="webfonts/fa-solid-900.woff2">
     <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff2" href="webfonts/fa-brands-400.woff2">
     <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff2" href="webfonts/fa-regular-400.woff2">
     <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff" href="webfonts/linea-arrows-10.woff">
<!-- 
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
     -->

    <!-- Template color -->
    <!-- <link href="css/color-variations/orange.css" rel="stylesheet" type="text/css" media="screen"> -->
</head>