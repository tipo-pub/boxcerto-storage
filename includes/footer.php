        <!-- Footer -->
        <footer id="footer" class="footer-light">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Footer widget area 1 -->
                            <div class="widget clearfix widget-contact-us" style="background-image: url('images/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat">
                                <h4 class="text-light">Contato</h4>
                                <ul class="list-icon">
                                    <li><i class="fa fa-map-marker-alt"></i> <a href="<?=$linkMaps;?>" title="<?=$endereco;?> - <?=$cidade;?>"><?=$endereco;?> - <?=$cidade;?> CEP: <?=$cep;?></a></li>
                                    <li><i class="fa fa-phone"></i> <a href="<?=$linkTel;?>" title="(<?=$ddd;?>) <?=$tel;?>">(<?=$ddd;?>) <?=$tel;?></a></li>
                                    <li><i class="fa fa-phone"></i> <a href="<?=$linkTel2;?>" title="(<?=$ddd;?>) <?=$tel2;?>">(<?=$ddd;?>) <?=$tel2;?></a> </li>
                                    <li><i class="far fa-envelope"></i> <a href="<?=$linkEmail;?>" title="<?=$email;?>"><?=$email;?></a>
                                    </li>
                                </ul>
                                <p class="text-light"><i class="far fa-clock"></i> <?=$horarioFunc;?></p>
                            </div>
                            <!-- end: Footer widget area 1 -->
                        </div>
                        <div class="col-lg-3">
                            <!-- Footer widget area 2 -->
                            <div class="widget">
                                <h4 class="text-light">Links</h4>
                                <ul class="list-icon list-icon-arrow">
                                    <li><a href="<?=$url;?>empresa" title="Empresa">Empresa</a></li>
                                     <li> <a href="<?=$url;?>armazenamento-para-voce" title="Para Você">Soluções para você</a>
                                    </li>
                                     <li>
                                      <a href="<?=$url;?>armazenamento-para-empresa" title="Para sua Empresa">Soluções para sua empresa</a>
                                  </li>

                                    <li><a href="<?=$url;?>medidas" title="Medidas">Medidas</a></li>
                                    <li><a href="<?=$url;?>galeria" title="Galeria">Galeria</a></li>
                                    <li><a href="<?=$url;?>contato" title="Contato">Contato</a></li>
                                    <li><a href="<?=$url;?>mapa-site" title="Mapa do Site">Mapa do Site</a></li>
                                </ul>
                            </div>
                            <!-- end: Footer widget area 2 -->
                        </div>
                        <div class="col-lg-3">
                            <!-- Footer widget area 3 -->
                            <div class="widget">
                                <h4 class="text-light">Redes Sociais</h4>

                                <!-- Social icons -->
                                <div class="social-icons social-icons-border float-left m-t-20">
                                    <ul>
                                        <li class="social-facebook"><a href="<?=$linkface;?>"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="social-youtube"><a href="<?=$linkYoutube;?>"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                                <!-- end: Social icons -->

                            </div>
                            <!-- end: Footer widget area 3 -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-content mt-4 pb-0">
                <div class="container">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="copyright-text text-dark"><strong>Copyright &copy; <?php echo date("Y") . '&nbsp;&nbsp;' . $NomeEmpresa ?> | Todos os Direitos Reservados</strong>
                            </div>
                        </div>

                        <div class="col-md-4 text-lg-right">
                            <p class=" text-dark"><strong>Desenvolvido por:</strong> <a target="_blank" href="http://www.tipopublicidade.com.br" title="Tipo Publicidade"><img src="images/tipo.png" style="width:30px" alt="Tipo Publicidade"></a></p>
                        </div>
                    </div>


                </div>
            </div>
        </footer>

        <?php include 'includes/btn-lateral.php' ;?>
        <!-- end: Footer -->

        </div>
        <!-- end: Wrapper -->

        <!-- Go to top button -->
        <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
        
        <script src="dist/app.js"></script>
        <script src="https://www.google.com/recaptcha/api.js"></script>

        </body>

        </html>
