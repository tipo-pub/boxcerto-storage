        <!-- Topbar -->
        <div id="topbar" class="topbar-fullwidth d-none d-xl-block d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-10">
                        <ul class="top-menu">
                            <li><a target="_blank" href="<?=$linkMaps;?>" title="<?=$endereco;?> - <?=$cidade;?>"> <i class="fa fa-map-marker-alt"></i> <?=$endereco;?> - <?=$cidade;?></a></li>
                            <li><a href="<?=$linkTel;?>" title="(<?=$ddd;?>) <?=$tel;?>"><i class="fa fa-phone"></i> (<?=$ddd;?>) <?=$tel;?></a></li>
                            <li><a href="<?=$linkTel2;?>" title="(<?=$ddd;?>) <?=$tel2;?>"><i class="fa fa-phone"></i> (<?=$ddd;?>) <?=$tel2;?></a></li>
                            <li><a href="<?=$linkEmail;?>" title="<?=$email;?>"><i class="fas fa-envelope"></i> <?=$email;?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 d-none d-sm-block">
                        <div class="social-icons social-icons-colored-hover">
                            <ul>
                                <li class="social-facebook"><a target="_blank" href="<?=$linkface;?>"><i class="fab fa-facebook-f"></i></a></li>                                
                                <li class="social-youtube"><a href="<?=$linkYoutube;?>"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Topbar -->