<?php
include "includes/geral.php";
$title = 'Para sua empresa';
$description = '';
$keywords = '';
include "includes/head.php";
include "includes/header.php";   
    // include "includes/slider.php";   
?>
<section id="solutions">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 pb-5">
        <div class="slider-solutions">
          <img class="" src="images/para-empresa/descarga.jpg" alt="">
          <img class="" src="images/para-empresa/corredor-1-andar-carrinho.jpg" alt="">
          <img class="" src="images/para-empresa/corredor-janela-recepcao.jpg" alt="">
          <img class="" src="images/para-empresa/corredor-terreo.jpg" alt="">
          <img class="" src="images/para-empresa/frente-da-empresa-parte-interna.jpg" alt="">
        </div>
      </div>
      <div class="col-12 col-lg-6 pl-5">
        <h3>Soluções em Self Storage / Guarda Móveis</h3>
        <p>O Self Storage / Guarda Móveis também é utilizado para armazenamento de documentos, arquivo morto, mercadorias, material promocional, material de eventos, entre outros itens.</p>

        <h4>Vantagens</h4>
        <ul>
          <li>Espaço físico, seguro e de fácil acesso;</li>
          <li>Alternativa prática e econômica;</li>
          <li>O usuário não se preocupa com manutenção, limpeza, vigilância, impostos, energia, água, nem taxa de condomínio;</li>
          <li>Contrato sem burocracia e sem fiador;</li>
          <li>Dedetização periódica;</li>
          <li>Controle de acesso por biométrico e cartão RFID.</li>
        </ul>

      </div>
    </div>
  </div>

</section>



<?php include 'includes/footer.php' ;?>
