<section class="background-grey depoimentos">

    <div class="container">

        <h3 class="text-center">Depoimentos</h3>

        <div class="carousel arrows-visibile testimonial testimonial-single testimonial-blockquote " data-items="1" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-loop="true">

            <!-- Testimonials item -->
            <div class="testimonial-item">
                <p>Nunca tinha necessitado de armazenamento fora da minha empresa, fique surpreso com a qualidade dos serviços prestados</p>
                <span>Gilberto, Lokaunet Studio</span>
            </div>
            <!-- end: Testimonials item-->

            <!-- Testimonials item -->
            <div class="testimonial-item">
                <p>Ficamos sem espaço em casa e resolvemos usar o sistema de storage, agora temos espaço em casa e com as nossas coisas muito bem guardadas.</p>
                <span>Tânia, Locadora</span>
            </div>
            <!-- end: Testimonials item-->

            <!-- Testimonials item -->
            <div class="testimonial-item">
                <p>Gostamos do suporte e informações a respeito de como locar e qual tamanho correto para alocar nossas coisas. </p>
                <span>Paulo, DKC</span>
            </div>
            <!-- end: Testimonials item-->

        </div>

    </div>

</section>
