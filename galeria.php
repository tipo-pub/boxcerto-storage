<?php
include "includes/geral.php";
$title = 'Galeria';
$description = '';
$keywords = '';
include "includes/head.php";
include "includes/header.php";   
    // include "includes/slider.php
?>

<section id="galeria">
  <div class="container pt-5">
   <!-- Gallery -->
   <div class="grid-layout grid-3-columns" data-margin="20" data-item="grid-item" data-lightbox="gallery">


     <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-01.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-01.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Biometria Exterior - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-01.jpg" class=" text-white text-uppercase text-gallery"><strong>Biometria Exterior</strong></a>

        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-02.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-02.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Monitoramento por câmeras - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-02.jpg" class=" text-white text-uppercase text-gallery"><strong>Monitoramento por câmeras</strong></a>

        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-03.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-03.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Corredor dos boxes - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-03.jpg" class=" text-white text-uppercase text-gallery"><strong>Corredor dos boxes</strong></a>
        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-04.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-04.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Corredor dos boxes - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-04.jpg" class=" text-white text-uppercase text-gallery"><strong>Corredor dos boxes</strong></a>
        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-05.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-05.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Corredor dos boxes - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-05.jpg" class=" text-white text-uppercase text-gallery"><strong>Corredor dos boxes</strong></a>
        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-06.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-06.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Corredor dos boxes - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-06.jpg" class=" text-white text-uppercase text-gallery"><strong>Corredor dos boxes</strong></a>
        </div>
      </div>
    </div>

    <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-07.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-07.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Parte interna da empresa - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-07.jpg" class=" text-white text-uppercase text-gallery"><strong>Parte interna da empresa</strong></a>
        </div>
      </div>
    </div>

     <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-08.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-08.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Recepção - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-08.jpg" class=" text-white text-uppercase text-gallery"><strong>Recepção</strong></a>
        </div>
      </div>
    </div>

         <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media grid-item">
      <div class="portfolio-item-wrap">
        <div class="portfolio-image">
          <a class="image-hover-zoom" href="images/galeria/thumbs/galeria-09.jpg" data-lightbox="gallery-item">
            <img src="images/galeria/thumbs/galeria-09.jpg">
          </a>
        </div>
        <div class="portfolio-description">
          <a title="Recepção - BoxCerto Storage" data-lightbox="image" href="images/galeria/galeria-09.jpg" class=" text-white text-uppercase text-gallery"><strong>Recepção</strong></a>
        </div>
      </div>
    </div>


  </div>
  <iframe width="1126" height="633" src="https://www.youtube.com/embed/FzYHf2l5OAM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

</section>



<?php include 'includes/footer.php' ;?>
