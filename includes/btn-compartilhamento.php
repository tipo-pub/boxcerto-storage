<script type="text/javascript">
	function popup_centralizado(pageURL, title,w,h) {
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	}
</script>
<div class="col-12 pb-3">

	<div class="buttons-share d-flex float-right">
		<a class="btn btn-xs btn-slide btn-twitter" onclick="popup_centralizado('https://twitter.com/intent/tweet?url=<?=$url.$urlPagina?>&amp;text=<?=str_replace(" ","%20","BoxCerto Storage ".' - '.$title);?>','Compartilhar no Twitter',700,430)" data-width="100">
			<i class="fab fa-twitter"></i>
			<span>Twitter</span>
		</a>

		<a class="btn btn-xs btn-slide btn-facebook" onclick="popup_centralizado('https://www.facebook.com/sharer/sharer.php?u=<?=$url.$urlPagina?>','Compartilhar no Facebook',700,315)">
			<i class="fab fa-facebook-f face-icon"></i>
			<span>Facebook</span>
		</a>

	</div>
	
	<?php /*
	<a class="btn btn-xs btn-slide btn-instagram" href="#" data-width="118">
		<i class="fab fa-instagram"></i>
		<span>Instagram</span>
	</a>
	<a class="btn btn-xs btn-slide btn-googleplus" href="mailto:#" data-width="80">
		<i class="far fa-envelope"></i>
		<span>Mail</span>
	</a>
	*/?>
</div>