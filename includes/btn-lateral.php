
<div id="ssb-container" class="ssb-btns-right ssb-anim-icons" style="z-index: 13">
    <ul class="ssb-dark-hover">
	    <li id="ssb-btn-0">
            <p>
                <a href="<?=$linkTel;?>"><span class="fa fa-phone"></span>  &nbsp; </a>
            </p>
        </li>
		 <li id="ssb-btn-2">
            <p>
                <a href="<?=$linkEmail;?>"><span class="fa fa-envelope"></span>  &nbsp; </a>
            </p>
        </li>
	</ul>
</div>
