<?php
include "includes/geral.php";
$title = 'Contato';
$description = '';
$keywords = '';
include "includes/head.php";
include "includes/header.php";   
    // include "includes/slider.php";   
?>
<section id="contact">
  <div class="container">
    <div class="col-12">
      <div class="row">
        <div class="col-12 col-lg-7">
          <h3>Fale conosco</h3>
          <form method="post">
            <div class="row">
              <div class="col-12 col-lg-4">
               <div class="form-group">
                 <label for="nome">Nome</label>
                 <input type="text" class="form-control" name="nome" placeholder="Nome completo* " required value="<?php echo isset($_POST['nome']) && !empty($_POST['nome']) ? $_POST['nome'] : '';?>">
               </div>
             </div>
             <div class="col-12 col-lg-4">
              <div class="form-group">
               <label for="email">E-mail</label>
               <input type="mail" class="form-control"  name="email" placeholder="Seu e-mail* " required value="<?php echo isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : '';?>" > 
             </div>
           </div>
           <div class="col-12 col-lg-4">
            <div class="form-group">
             <label for="telefone">Telefone</label>
             <input type="phone" class="form-control"  name="telefone" pattern=".{16,17}" required title="Insira um número de celular ou telefone." placeholder="(DDD) + Telefone* " required value="<?php echo isset($_POST['telefone']) && !empty($_POST['telefone']) ? $_POST['telefone'] : '';?>"> 
           </div>
         </div>
         <div class="col-12 col-lg-12">
          <div class="form-group">
           <label for="mensagem">Mensagem</label>
           <textarea class="form-control" name="mensagem" id="" cols="30" rows="10" placeholder="Digite sua mensagem*" required value="<?php echo isset($_POST['mensagem']) && !empty($_POST['mensagem']) ? $_POST['mensagem'] : '';?>"></textarea>
         </div>

       </div>

       <div class="form-group col-md-6">
    <div class="g-recaptcha" data-sitekey="<?=$sitekey?>"></div>
      </div>

       <div class="form-group col-md-6">
             <button type="submit" class="btn btn-rounded btn-outline float-right" name="submit" value="submit">Enviar mensagem</button>
                 <?php if(isset($_POST['submit'])){ require_once('php/cadastro-form.php'); } ?>
      </div>
    </div>

   

  </form>
</div>
<div class="col-12 col-lg-5">
 <h3>Endereço</h3>
 <p class="mb-3">Rua João Nascimento, 74 - Jardim Monte Alegre - Butantã - São Paulo • SP - CEP 05545-110</p>
 <div class="phones">
  <p class="m-0"><strong><a href="#">(11) 3782-7868</a> | <a href="#">(11) 2309-1628</a></strong></p>
  <p><strong><a href="#">(11) 97737-2705 WhatsApp</a></strong></p>
</div>
<a href="mailto:atendimento@boxcertostorage.com.br">atendimento@boxcertostorage.com.br</a>

<p class="mb-0">Seg. à Sexta: 8h30 às 17h30</p>
<p>Aos sábados: 08h30 às 12h30 (mediante agendamento prévio)</p>
</div>
</div>
</div>
</div>

<div class="container-fluid text-center p-0">

  <iframe class="pt-3 pb-4" width="100%" height="300" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=Rua%20Jo%C3%A3o%20Nascimento%2C%2074%20-%20Jardim%20Monte%20Alegre+(Localiza%C3%A7%C3%A3o)&amp;ie=UTF8&amp;t=&amp;z=16&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

  <div class="content-local p-3">
    <p><strong>Estamos localizados no Butantã, zona oeste de São Paulo, na altura do km 15 da Rodovia Raposo Tavares.</strong></p>
    <p><strong>Também estamos próximos ao Rodoanel.</strong></p>
  </div>
</div>
</section>



<?php include 'includes/footer.php' ;?>
