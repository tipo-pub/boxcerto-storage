<?php
    include "includes/geral.php";
    $title = 'Home';
    $description = '';
    $keywords = '';
    include "includes/head.php";
    include "includes/header.php";   
    include "includes/slider.php";   
?>

<!-- INFO HOME -->
<section class="p-t-150 p-b-100">
    <div class="container">
        <div class="col-lg-8 m-b-100 center">
            <div class="heading section-title">
                <h2>Precisando de espaço?</h2>
                <p>Somos uma empresa de Self Storage / Guarda Móveis, onde você pode alugar boxes de 2 a 6 m² para guardar móveis, mercadorias, documentos e muito mais. A contratação é simples, sem burocracia, e pelo tempo que você achar necessário.</p>
            </div>
        </div>


        <div class="row">

            <div class="col-lg-4">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-box"></i></a>
                    </div>
                    <h3>O que é Self Storage / Guarda Móveis?</h3>
                    <p class="lead">Modelo de armazenamento onde você pode locar seu espaço para guardar móveis, documentos, volumes, etc...</p>
                    <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-tags"></i></a>
                    </div>
                    <h5>Promoção</h5>
                    <p class="lead">Lançamento de boxes De 3m² e 4m² à preços imbatíveis!</p>
                    <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-user-shield"></i></a>
                    </div>
                    <h5>Segurança 24 horas</h5>
                    <p class="lead">Monitoramento por câmeras 24 horas por dia, controle de pragas e insetos, garantindo a segurança dos pertences ...</p>
                    <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: INFO HOME -->

<!-- BANNER DESCONTO -->
<section class="row background-blue text-center banner-desconto">
    <div class="container">
        <h3>TODOS OS BOXES COM 50% DE DESCONTO</h3>

        <p class="text-light">*Durante os 3 primeiros meses de locação</p>


        <a href="<?=$url;?>#" class="btn2"><span>Saiba Mais</span></a>
    </div>
</section>
<!-- / BANNER DESCONTO -->


<!-- ESCOLHA O BOX -->
<?php include 'includes/medidas.php' ;?>
<!-- / ESCOLHA O BOX -->


<!-- INFORMAÇÕES -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 p-t-80">
                <div class="icon-box icon-box-right effect medium border small" data-animation="fadeInLeft" data-animation-delay="200">
                    <div class="icon">
                        <i class="fas fa-boxes"></i>
                    </div>
                    <h3>Várias opções de box</h3>
                    <p> Boxes de 2 a 6m² para guardar tudo que precisar. </p>
                </div>
                <div class="icon-box icon-box-right effect medium border small" data-animation="fadeInLeft" data-animation-delay="400">
                    <div class="icon">
                        <i class="fas fa-user-lock"></i>
                    </div>
                    <h3>Privacidade</h3>
                    <p> Somente você ou pessoas autorizadas por você terão acesso ao box</p>
                </div>
            </div>

            <div class="col-lg-4 text-center"data-animation-delay="100">
                <img alt="img" class="img-fluid center-block" src="images/img-caixas.jpg" loading="lazy">
            </div>

            <div class="col-lg-4 p-t-80">

                <div class="icon-box effect medium border small" data-animation="fadeInRight" data-animation-delay="400">
                    <div class="icon">
                        <i class="fas fa-edit"></i>
                    </div>
                    <h3>Contratação simples</h3>
                    <p> Contratação rápida, sem burocracia e sem fiador; </p>
                </div>

                <div class="icon-box effect medium border small" data-animation="fadeInRight" data-animation-delay="600">
                    <div class="icon">
                        <i class="fas fa-user-edit"></i>
                    </div>
                    <h3>Controle de acesso</h3>
                    <p> Acesso por bimetria e cartão RFID </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- / INFORMAÇÕES -->

<?php include 'includes/depoimentos.php' ;?>

<?php include 'includes/footer.php' ;?>
