<body>

    <!-- Wrapper -->
    <div id="wrapper">
        
        <?php include 'includes/top-bar.php' ;?>

        <!-- Header -->
        <header id="header" class="header-transparent header-fullwidth">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="<?=$url;?>" class="logo" data-dark-logo="<?=$logotipo;?>">
                            <img src="<?=$logotipo;?>" alt="<?=$NomeEmpresa;?>" loading="lazy">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="<?=$url;?>" title="Home">Home</a></li>
                                    <li><a href="<?=$url;?>empresa" title="Empresa">Empresa</a></li>
                                    
                                    <li class="dropdown"> <a href="#" title="Soluções">Soluções</a>
                                        <ul class="dropdown-menu">
                                            <li> <a href="<?=$url;?>armazenamento-para-voce" title="Para Você">Para Você</a></li>
                                            <li> <a href="<?=$url;?>armazenamento-para-empresa" title="Para sua Empresa">Para sua Empresa</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?=$url;?>medidas" title="Medidas">Medidas</a></li>
                                    <li><a href="<?=$url;?>galeria" title="Galeria">Galeria</a></li>
                                    <li><a href="<?=$url;?>contato" title="Contato">Contato</a></li>
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
            <?php include "includes/banner-breadcrumb.php";?>