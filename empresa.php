<?php
include "includes/geral.php";
$title = 'Empresa';
$description = '';
$keywords = '';
include "includes/head.php";
include "includes/header.php";   
    // include "includes/slider.php";   
?>

<!-- INFO HOME -->
<section id="empresa">
    <div class="container">
     
        <div class="heading section-title mb-2">
            <h2><strong>Precisando de espaço?</strong></h2>
        </div>
        
        <div class="row">
            <div class="col-lg-9 pb-lg-5">
                <div class="main">
                    <p>Somos uma empresa de Self Storage / Guarda Móveis, onde você pode alugar boxes de 2 a 6 m² para guardar móveis, mercadorias, documentos e muito mais. A contratação é simples, sem burocracia, e pelo tempo que você achar necessário.</p>
                    <p>Nosso Self Storage / Guarda Móveis está localizado na zona oeste de São Paulo, no km 15 da Rodovia Raposo Tavares. O acesso à empresa é muito fácil, ampla área para estacionamento e plataforma de carga e descarga. Temos a estrutura ideal para guardar suas mercadorias por um ótimo custo/benefício.</p>
                    <p>Nossa empresa é monitorada por câmeras, 24 horas por dia, controle de pragas e insetos, garantindo a segurança dos pertences de nossos clientes.</p>
                    <p>Alugue um box em nosso Self Storage / Guarda Móveis e tenha uma extensão de sua casa ou de sua empresa!</p>
                </div>
            </div>
            <div class="col-lg-3 pb-lg-5">
                <img src="images/garotinha.jpg" alt="">
            </div>
            <div class="col-12">   
                <div class="heading section-title">
                    <h3>Qualidades BoxCerto Storage</h3>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-concierge-bell"></i></a>
                    </div>
                    <h3>Atendimento Especializado</h3>
                    <p class="lead">Modelo de armazenamento onde você pode locar seu espaço para guardar móveis, documentos, volumes, etc...</p>
                    <!--  <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a> -->
                </div>
            </div>
            <div class="col-lg-3 text-center">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-chart-line"></i></a>
                    </div>
                    <h5>Monitoramento</h5>
                    <p class="lead">Lançamento de boxes De 3m² e 4m² à preços imbatíveis!</p>
                    <!-- <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-chart-area"></i></a>
                    </div>
                    <h5>Instalações</h5>
                    <p class="lead">Monitoramento por câmeras 24 horas por dia, controle de pragas e insetos, garantindo a segurança dos pertences ...</p>
                    <!-- <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="icon-box text-center effect large light">
                    <div class="icon">
                        <a href="<?=$url;?>#"><i class="fas fa-universal-access"></i></a>
                    </div>
                    <h5>Facilidades</h5>
                    <p class="lead">Monitoramento por câmeras 24 horas por dia, controle de pragas e insetos, garantindo a segurança dos pertences ...</p>
                    <!-- <a href="<?=$url;?>#"><button type="button" class="btn btn-rounded btn-outline">Leia Mais</button></a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<section id="galeria">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 pb-3">
                <img class="img-fluid w-100" src="images/empresa/corredor-janela-recepcao.jpg" alt="" loading="lazy">
            </div>
            <div class="col-12 col-lg-3 pb-3">
                <img class="img-fluid w-100" src="images/empresa/corredor-terreo.jpg" alt="" loading="lazy">

            </div>
            <div class="col-12 col-lg-3 pb-3">
                <img class="img-fluid w-100" src="images/empresa/frente-empresa-interna.jpg" alt="" loading="lazy">

            </div>
            <div class="col-12 col-lg-3 pb-3">
                <img class="img-fluid w-100" src="images/empresa/recepcao-frente.jpg" alt="" loading="lazy">
            </div>
        </div>
    </div>
</section>
<!-- end: INFO HOME -->


<?php include 'includes/footer.php' ;?>
