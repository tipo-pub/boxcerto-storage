// unique scripts go here.
function putMask(){
	$('input[type="phone"]').mask('(000) 0 0000-0000');
}

function loadSlider(){
	$('.slider-solutions').slick({
		arrows: false,
		fade: true,
		autoplay: true,
		infinite: true
	});


	$('.slider-items').slick({
		 prevArrow: $('.prev-btn'),
		 nextArrow: $('.next-btn'),
		 // arrows: false,
		 //fade: true,
		 slidesToShow: 4,
  		 slidesToScroll: 1,
		 autoplay: true,
		 infinite: true,
		 responsive:[{
			breakpoint: 600,
			settings:{
				slidesToShow: 1,
				slidestToScroll: 1,
				autoplay: true
			}

		}]
	});

}
