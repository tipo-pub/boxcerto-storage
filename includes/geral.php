<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR">
    <!--<![endif]-->

<?php

// URL Automática
$pastaEPagina = explode("/", $_SERVER['PHP_SELF']);
$pastaDominio = "";
for ($i = 0; $i < count($pastaEPagina); $i++) {
    if (substr_count($pastaEPagina[$i], ".") == 0) {
        $pastaDominio .= $pastaEPagina[$i] . "/";
    }
}

include "includes/functions.php";

$http = ($_SERVER['HTTP_HOST'] == "localhost") || ($_SERVER['HTTP_HOST'] == "www.tipotemporario.com.br") || ($_SERVER['HTTP_HOST'] == "tipotemporario.com.br") || (empty($_SERVER['HTTPS'])) ? "http://" : "https://" ;
$url = $http . $_SERVER['HTTP_HOST'] . $pastaDominio;
$urlPagina = str_replace($pastaDominio, '', $_SERVER['PHP_SELF']);
// $urlPagina = str_replace('.php', '', $urlPagina);
//
$NomeEmpresa = 'BoxCerto Storage';
$author = 'www.tipopublicidade.com.br';
$ramo = 'Aluguel de Self Storage / Guarda Móveis';
$email = 'atendimento@boxcertostorage.com.br';
$linkEmail = 'mailto:atendimento@boxcertostorage.com.br';
$ddd = '11';
$tel = '3782-7868';
$linkTel = 'tel:1137827868';
$tel2 = '2309-1628';
$linkTel2 = 'tel:1123091628';
//$linkWhats = 'http://api.whatsapp.com/send?1=pt_BR&amp;phone=55ddd-telefone';
$endereco = 'Rua João Nascimento, 74 - Vila Borges - Butantã';
//$bairro = 'bairro';
$cidade = 'São Paulo - SP';
$uf = 'São Paulo - SP';
$cep = '05545-110';
    
$linkMaps = 'https://goo.gl/maps/yxCfWTEMXmSkmgzm9';
$mapa = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7312.915440644509!2d-46.767375!3d-23.587912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce55b61c36a3f9%3A0xa287095582018776!2sR.%20Jo%C3%A3o%20Nascimento%2C%2074%20-%20Jardim%20Monte%20Alegre%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005545-110!5e0!3m2!1spt-BR!2sbr!4v1572270252517!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';

//Horário de Funcionamento
$horarioFunc = 'Seg. à Sexta - 8:30hs às 17:30hs / Sábado - 08:30hs às 12:30hs';
    
$logotipo = 'images/boxcertostorage.png';
$creditos = 'Tipo Publicidade';
$canonical = $url . $urlPagina;
$canonical = str_replace('.php', '', $canonical);
$imagem = str_replace('.php', '.jpg', $urlPagina); //imagem das páginas das palavras-chave
//redes sociais
$card = $url . 'images/card.jpg';
$CodFanpage = '1503960436491003';

//Links icones redes sociais
$linkface = 'https://www.facebook.com/boxcerto';
$linkYoutube = 'https://www.youtube.com/channel/UCTR6RitCaGmALg2ASIIKJjg';

$urlPagina          = explode("/", $_SERVER['PHP_SELF']);
$urlPagina          = end($urlPagina);
$linkPagina         = str_replace('.php', '', $urlPagina);
$canonical          = $url . $linkPagina;
$imagem             = str_replace('.php', '.jpg', $urlPagina); //imagem das páginas das palavras-chave




//ReCaptcha Google
$sitekey = "6LfuYlIUAAAAAHcbP_FsKqmBMHXb4OMk6NpPxo3O";
$secret_key = '6LfuYlIUAAAAAGphR1_KNoeaR_yalNludzCSip1G';




// Palavras chave (tags)
$LinksPalavras = array(
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
    "exemplo-palavra-chave" => "Exemplo Palavra Chave",
);
?>
